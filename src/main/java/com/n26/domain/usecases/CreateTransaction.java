package com.n26.domain.usecases;

import com.n26.domain.model.Transaction;
import com.n26.domain.usecases.exceptions.BusinessException;
import com.n26.domain.usecases.exceptions.NoContentException;
import com.n26.domain.usecases.gateways.StatisticsGateway;
import com.n26.domain.usecases.validators.TimestampValidator;
import org.springframework.stereotype.Service;

import java.time.Clock;

@Service
public class CreateTransaction {

    private final StatisticsGateway statisticsGateway;
    private final Clock clock;

    public CreateTransaction(final StatisticsGateway statisticsGateway, final Clock clock) {
        this.statisticsGateway = statisticsGateway;
        this.clock = clock;
    }

    public void create(final Transaction transaction) {
        final TimestampValidator validator = TimestampValidator.create(transaction.getTimestamp(), this.clock);

        if (validator.isInTheFuture()) {
            throw new BusinessException("Time must not be in the future");
        }

        if (validator.isTooOld()) {
            throw new NoContentException("Transaction timestamp cannot be older than 60 seconds");
        }

        this.statisticsGateway.save(transaction);
    }

}
