package com.n26.domain.usecases.validators;

import java.time.Clock;
import java.time.LocalDateTime;

public class TimestampValidator {

    private final LocalDateTime timestamp;
    private final Clock clock;

    private TimestampValidator(final LocalDateTime timestamp, final Clock clock) {
        this.timestamp = timestamp;
        this.clock = clock;
    }

    public static TimestampValidator create(final LocalDateTime timestamp, final Clock clock) {
        return new TimestampValidator(timestamp, clock);
    }

    public boolean isTooOld() {
        final LocalDateTime now = LocalDateTime.now(this.clock);
        final LocalDateTime aMinuteBefore = now.minusSeconds(60);
        return this.timestamp.isBefore(aMinuteBefore);
    }

    public boolean isInTheFuture() {
        final LocalDateTime now = LocalDateTime.now(this.clock);
        return this.timestamp.isAfter(now);
    }

}