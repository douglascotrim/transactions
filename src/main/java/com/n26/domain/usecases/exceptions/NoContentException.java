package com.n26.domain.usecases.exceptions;

public class NoContentException extends RuntimeException {

    private static final long serialVersionUID = -7287977181265801984L;

    public NoContentException(final String message) {
        super(message);
    }

}
