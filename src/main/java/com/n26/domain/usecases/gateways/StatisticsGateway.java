package com.n26.domain.usecases.gateways;

import com.n26.domain.model.Statistics;
import com.n26.domain.model.Transaction;

import java.time.LocalDateTime;

public interface StatisticsGateway {

    /**
     * Save the transaction to further statistics calculation.
     *
     * @param transaction the transaction carrying the amount and timestamp.
     */
    void save(Transaction transaction);

    /**
     * Get the statistics of the current time (now).
     *
     * @return
     */
    Statistics get(LocalDateTime timestamp);

    /**
     * Delete all transactions.
     */
    void clear();

}
