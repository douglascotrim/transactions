package com.n26.domain.usecases;

import com.n26.domain.usecases.gateways.StatisticsGateway;
import org.springframework.stereotype.Service;

@Service
public class DeleteAllTransactions {

    private final StatisticsGateway statisticsGateway;

    public DeleteAllTransactions(final StatisticsGateway statisticsGateway) {
        this.statisticsGateway = statisticsGateway;
    }

    public void deleteAll() {
        this.statisticsGateway.clear();
    }

}
