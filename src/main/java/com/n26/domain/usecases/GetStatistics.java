package com.n26.domain.usecases;

import com.n26.domain.model.Statistics;
import com.n26.domain.usecases.gateways.StatisticsGateway;
import org.springframework.stereotype.Service;

import java.time.Clock;
import java.time.LocalDateTime;

@Service
public class GetStatistics {

    private final StatisticsGateway statisticsGateway;
    private final Clock clock;

    public GetStatistics(final StatisticsGateway statisticsGateway, final Clock clock) {
        this.statisticsGateway = statisticsGateway;
        this.clock = clock;
    }

    public Statistics get() {
        return this.statisticsGateway.get(LocalDateTime.now(this.clock));
    }

}
