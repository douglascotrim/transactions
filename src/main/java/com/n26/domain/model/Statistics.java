package com.n26.domain.model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;
import java.util.Optional;

public class Statistics {

    private BigDecimal sum;
    private BigDecimal avg;
    private BigDecimal max;
    private BigDecimal min;
    private BigDecimal count;

    private Statistics() {
        this.count = new BigDecimal(0);
        this.sum = new BigDecimal(0);
        this.avg = new BigDecimal(0);
        this.max = new BigDecimal(0);
        this.min = null;
    }

    public static Statistics create() {
        return new Statistics();
    }

    public static Statistics create(final BigDecimal amount) {
        return new Statistics().add(amount);
    }

    public BigDecimal getCount() {
        return this.count;
    }

    public BigDecimal getSum() {
        return this.sum;
    }

    public BigDecimal getAvg() {
        return this.avg;
    }

    public BigDecimal getMax() {
        return this.max;
    }

    public BigDecimal getMin() {
        return Optional.ofNullable(this.min).orElse(BigDecimal.ZERO);
    }

    public Statistics add(final BigDecimal amount) {
        this.count = this.count.add(BigDecimal.ONE);
        this.sum = this.sum.add(amount);
        this.avg = this.sum.divide(this.count, 2, RoundingMode.HALF_EVEN);
        this.max = this.max.max(amount);
        this.min = Optional.ofNullable(this.min).map(m -> m.min(amount)).orElse(amount);
        return this;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Statistics that = (Statistics) o;
        return this.count.equals(that.count) &&
                this.sum.equals(that.sum) &&
                this.avg.equals(that.avg) &&
                this.max.equals(that.max) &&
                this.min.equals(that.min);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.count, this.sum, this.avg, this.max, this.min);
    }

    @Override
    public String toString() {
        return "Statistics{" + "count=" + this.count + ", sum=" + this.sum + ", avg=" + this.avg + ", max="
                + this.max + ", min=" + this.min + '}';
    }

}
