package com.n26.domain.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.Optional;

public class Transaction {

    private final BigDecimal amount;
    private final LocalDateTime timestamp;

    public Transaction(final BigDecimal amount, final LocalDateTime timestamp) {
        this.amount = amount;
        this.timestamp = timestamp;
    }

    public static Optional<Transaction> from(final String amount, final String timestamp) {
        try {
            final BigDecimal convertedAmount = BigDecimal.valueOf(Double.valueOf(amount));
            final LocalDateTime convertedTimestamp = LocalDateTime.parse(timestamp, DateTimeFormatter.ISO_DATE_TIME);
            return Optional.of(new Transaction(convertedAmount, convertedTimestamp));
        } catch (final RuntimeException e) {
            return Optional.empty();
        }
    }

    public BigDecimal getAmount() {
        return this.amount;
    }

    public LocalDateTime getTimestamp() {
        return this.timestamp;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Transaction that = (Transaction) o;
        return this.amount.equals(that.amount) &&
                this.timestamp.equals(that.timestamp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.amount, this.timestamp);
    }

    @Override
    public String toString() {
        return "Transaction{" + "amount=" + this.amount + ", timestamp=" + this.timestamp + '}';
    }

}

