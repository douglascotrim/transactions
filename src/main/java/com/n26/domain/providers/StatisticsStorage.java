package com.n26.domain.providers;

import com.n26.domain.model.Statistics;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class StatisticsStorage {

    private static final String FORMAT_UP_TO_SECONDS = "yyyy-MM-dd HH:mm:ss";
    private static final DateTimeFormatter keyFormat = DateTimeFormatter.ofPattern(FORMAT_UP_TO_SECONDS);

    private final Map<String, Statistics> statByTimestamp = new ConcurrentHashMap<>();

    public void save(final BigDecimal amount, final LocalDateTime timestamp) {
        final String key = getKey(timestamp);
        Statistics statistics = this.statByTimestamp.get(key);

        if (statistics == null) {
            statistics = Statistics.create(amount);
        } else {
            statistics = statistics.add(amount);
        }

        this.statByTimestamp.put(key, statistics);
    }

    private static String getKey(final LocalDateTime localDateTime) {
        return localDateTime.format(keyFormat);
    }

    public Optional<Statistics> get(final LocalDateTime timestamp) {
        return Optional.ofNullable(this.statByTimestamp.get(getKey(timestamp)));
    }

    public void clear() {
        this.statByTimestamp.clear();
    }

}
