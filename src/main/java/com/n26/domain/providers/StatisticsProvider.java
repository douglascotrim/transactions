package com.n26.domain.providers;

import com.n26.domain.model.Statistics;
import com.n26.domain.model.Transaction;
import com.n26.domain.usecases.gateways.StatisticsGateway;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.stream.IntStream;

@Service
public class StatisticsProvider implements StatisticsGateway {

    private static final int SECONDS = 60;

    private final StatisticsStorage storage;

    public StatisticsProvider(final StatisticsStorage storage) {
        this.storage = storage;
    }

    @Override
    public void save(final Transaction transaction) {
        final LocalDateTime timestamp = transaction.getTimestamp();
        IntStream.range(0, SECONDS).forEach(i -> this.storage.save(transaction.getAmount(), timestamp.plusSeconds(i)));
    }

    @Override
    public Statistics get(final LocalDateTime timestamp) {
        return this.storage.get(timestamp).orElse(Statistics.create());
    }

    @Override
    public void clear() {
        this.storage.clear();
    }

}
