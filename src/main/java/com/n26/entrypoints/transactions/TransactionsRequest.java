package com.n26.entrypoints.transactions;

public class TransactionsRequest {

    private String amount;
    private String timestamp;

    public TransactionsRequest() {
    }

    public TransactionsRequest(final String amount, final String timestamp) {
        this.amount = amount;
        this.timestamp = timestamp;
    }

    public String getAmount() {
        return this.amount;
    }

    public void setAmount(final String amount) {
        this.amount = amount;
    }

    public String getTimestamp() {
        return this.timestamp;
    }

    public void setTimestamp(final String timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "TransactionsRequest{" + "amount='" + this.amount + '\'' + ", timestamp='" + this.timestamp + '\'' + '}';
    }

}
