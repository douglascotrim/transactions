package com.n26.entrypoints.transactions;

import com.n26.domain.model.Transaction;
import com.n26.domain.usecases.CreateTransaction;
import com.n26.domain.usecases.DeleteAllTransactions;
import com.n26.domain.usecases.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TransactionsEntrypoint {

    private static final Logger LOGGER = LoggerFactory.getLogger(TransactionsEntrypoint.class);

    private final CreateTransaction createTransaction;
    private final DeleteAllTransactions deleteAllTransactions;

    public TransactionsEntrypoint(final CreateTransaction createTransaction, final DeleteAllTransactions deleteAllTransactions) {
        this.createTransaction = createTransaction;
        this.deleteAllTransactions = deleteAllTransactions;
    }

    @PostMapping("/transactions")
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody final TransactionsRequest request) {
        LOGGER.debug("Started Transactions Entrypoint: [POST][/transactions]");
        LOGGER.debug(String.format("Received Transaction %s", request));

        final Transaction transaction = Transaction.from(request.getAmount(), request.getTimestamp())
                .orElseThrow(() -> new BusinessException("Parse error"));

        this.createTransaction.create(transaction);

        LOGGER.debug("Finished Transactions Entrypoint: [POST][/transactions]");
    }

    @DeleteMapping("/transactions")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteAll() {
        LOGGER.debug("Started Transactions Entrypoint: [DELETE][/transactions]");

        this.deleteAllTransactions.deleteAll();

        LOGGER.debug("Finished Transactions Entrypoint: [DELETE][/transactions]");
    }

}
