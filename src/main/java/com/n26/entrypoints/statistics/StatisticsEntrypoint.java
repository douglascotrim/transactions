package com.n26.entrypoints.statistics;

import com.n26.domain.model.Statistics;
import com.n26.domain.usecases.GetStatistics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StatisticsEntrypoint {

    private static final Logger LOGGER = LoggerFactory.getLogger(StatisticsEntrypoint.class);

    private final GetStatistics getStatistics;

    public StatisticsEntrypoint(final GetStatistics getStatistics) {
        this.getStatistics = getStatistics;
    }

    @GetMapping("/statistics")
    public StatisticsResponse get() {
        LOGGER.debug("Started Statistics Entrypoint: [GET][/statistics]");

        final Statistics statistics = this.getStatistics.get();
        final StatisticsResponse response = StatisticsResponse.from(statistics);

        LOGGER.debug(String.format("Returning Statistics Entrypoint: [GET][/statistics] body: %s", response));

        return response;
    }

}
