package com.n26.entrypoints.statistics;

import com.n26.domain.model.Statistics;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;

public class StatisticsResponse {

    private String sum;
    private String avg;
    private String max;
    private String min;
    private long count;

    public StatisticsResponse() {
    }

    public static StatisticsResponse from(final Statistics statistics) {
        final StatisticsResponse response = new StatisticsResponse();
        response.sum = stringFromNumber(statistics.getSum());
        response.avg = stringFromNumber(statistics.getAvg());
        response.max = stringFromNumber(statistics.getMax());
        response.min = stringFromNumber(statistics.getMin());
        response.count = statistics.getCount().longValue();
        return response;
    }

    private static String stringFromNumber(final BigDecimal number) {
        return number.setScale(2, RoundingMode.HALF_EVEN).toString();
    }

    public String getSum() {
        return this.sum;
    }

    public void setSum(final String sum) {
        this.sum = sum;
    }

    public String getAvg() {
        return this.avg;
    }

    public void setAvg(final String avg) {
        this.avg = avg;
    }

    public String getMax() {
        return this.max;
    }

    public void setMax(final String max) {
        this.max = max;
    }

    public String getMin() {
        return this.min;
    }

    public void setMin(final String min) {
        this.min = min;
    }

    public long getCount() {
        return this.count;
    }

    public void setCount(final long count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "StatisticsResponse{" +
                "sum=" + this.sum + ", avg=" + this.avg + ", max=" + this.max + ", min=" + this.min +
                ", count=" + this.count + '}';
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final StatisticsResponse that = (StatisticsResponse) o;
        return this.count == that.count && Objects.equals(this.sum, that.sum) && Objects.equals(this.avg, that.avg)
                && Objects.equals(this.max, that.max) && Objects.equals(this.min, that.min);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.sum, this.avg, this.max, this.min, this.count);
    }

}
