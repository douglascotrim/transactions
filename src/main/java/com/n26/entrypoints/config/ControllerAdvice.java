package com.n26.entrypoints.config;

import com.n26.domain.usecases.exceptions.BusinessException;
import com.n26.domain.usecases.exceptions.NoContentException;
import com.n26.entrypoints.errors.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ControllerAdvice {

    private static final String BUSINESS_ERROR_CODE = "business_error";
    private static final String NO_CONTENT_ERROR_CODE = "no_content";

    @ExceptionHandler(BusinessException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    public ErrorResponse handle(final BusinessException exception) {
        return new ErrorResponse(BUSINESS_ERROR_CODE, exception.getMessage());
    }

    @ExceptionHandler(NoContentException.class)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ErrorResponse handle(final NoContentException exception) {
        return new ErrorResponse(NO_CONTENT_ERROR_CODE, exception.getMessage());
    }


}
