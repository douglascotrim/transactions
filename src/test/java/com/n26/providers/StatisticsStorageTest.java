package com.n26.providers;

import com.n26.domain.model.Statistics;
import com.n26.domain.providers.StatisticsStorage;
import com.n26.fixtures.LocalDateTimeFixture;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

public class StatisticsStorageTest {

    @Test
    public void shouldSaveAndGetAnStatistics() {
        final LocalDateTime dateTime = LocalDateTimeFixture.NOW.get();

        final StatisticsStorage statisticsStorage = new StatisticsStorage();
        statisticsStorage.save(BigDecimal.valueOf(12.12), dateTime);
        final Optional<Statistics> statistics = statisticsStorage.get(dateTime);

        assertThat(statistics).isNotEmpty();
        assertThat(statistics.get().getCount()).isEqualTo(BigDecimal.ONE);
    }

    @Test
    public void shouldSaveButNotGetStatistics() {
        final LocalDateTime dateTime = LocalDateTimeFixture.NOW.get();

        final StatisticsStorage statisticsStorage = new StatisticsStorage();
        statisticsStorage.save(BigDecimal.valueOf(12.12), dateTime);
        final Optional<Statistics> statistics = statisticsStorage.get(dateTime.plusSeconds(1));

        assertThat(statistics).isEmpty();
    }

}
