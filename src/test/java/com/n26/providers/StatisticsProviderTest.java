package com.n26.providers;

import com.n26.domain.model.Statistics;
import com.n26.domain.model.Transaction;
import com.n26.domain.providers.StatisticsProvider;
import com.n26.fixtures.LocalDateTimeFixture;
import com.n26.fixtures.TransactionFixture;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;

import static com.n26.fixtures.LocalDateTimeFixture.A_MINUTE_AHEAD;
import static com.n26.fixtures.LocalDateTimeFixture.A_MINUTE_BEFORE;
import static com.n26.fixtures.LocalDateTimeFixture.NOW;
import static com.n26.fixtures.LocalDateTimeFixture.THIRTY_SECONDS_BEFORE;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class StatisticsProviderTest {

    @Autowired
    private StatisticsProvider statisticsProvider;

    @Test
    public void shouldCalculateStatisticsForSixtySecondsAhead() {
        final Transaction first = TransactionFixture.NOW.get();
        final Transaction second = TransactionFixture.THIRTY_SECONDS_BEFORE.get();
        this.statisticsProvider.save(first);
        this.statisticsProvider.save(second);

        assertAt(A_MINUTE_BEFORE, 0);
        assertAt(THIRTY_SECONDS_BEFORE, 1);
        assertAt(NOW, 2);
        assertAt(A_MINUTE_AHEAD, 0);
    }

    private void assertAt(final LocalDateTimeFixture dateTimeSupplier, final long count) {
        final Statistics stats = this.statisticsProvider.get(dateTimeSupplier.get());
        assertThat(stats).isNotNull();
        assertThat(stats.getCount()).isEqualTo(BigDecimal.valueOf(count));
    }

}
