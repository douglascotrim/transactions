package com.n26.usecases;

import com.n26.Application;
import com.n26.domain.usecases.CreateTransaction;
import com.n26.domain.usecases.exceptions.BusinessException;
import com.n26.domain.usecases.exceptions.NoContentException;
import com.n26.fixtures.TransactionFixture;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;

@SpringBootTest
@Import(Application.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class CreateTransactionTest {

    @Configuration
    static class ClockConfig {
        @Bean
        public Clock clock() {
            return Clock.fixed(Instant.parse("2018-07-17T09:59:51.312Z"), ZoneId.of("UTC"));
        }
    }

    @Autowired
    private CreateTransaction createTransaction;

    /**
     * Should create transaction if it is not older than 60 seconds or in the future
     */
    @Test
    public void shouldCreateTransaction() {
        this.createTransaction.create(TransactionFixture.NOW.get());
    }

    /**
     * Should not create a transaction when it is older than 60 seconds
     */
    @Test(expected = NoContentException.class)
    public void shouldNotCreateTransactionOneMinuteBefore() {
        this.createTransaction.create(TransactionFixture.A_MINUTE_BEFORE.get());
    }

    /**
     * Should not create a transaction when it is in the future
     */
    @Test(expected = BusinessException.class)
    public void shouldNotCreateTransactionInTheFuture() {
        this.createTransaction.create(TransactionFixture.A_MINUTE_AHEAD.get());
    }

}
