package com.n26.domain;

import com.n26.domain.model.Transaction;
import com.n26.fixtures.LocalDateTimeFixture;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;

public class TransactionTest {

    @Test
    public void testParsing() {
        final Transaction transaction = Transaction.from("12.3343", "2018-07-17T09:59:51.312Z").get();
        final LocalDateTime dateTime = LocalDateTimeFixture.NOW.get();

        assertThat(transaction).isNotNull();
        assertThat(transaction.getAmount()).isEqualTo(BigDecimal.valueOf(12.3343));
        assertThat(transaction.getTimestamp()).isEqualTo(dateTime);
    }

}
