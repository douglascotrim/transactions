package com.n26.fixtures;

import com.n26.domain.model.Transaction;

import java.math.BigDecimal;
import java.util.function.Supplier;

public enum TransactionFixture implements Supplier<Transaction> {

    NOW {
        @Override
        public Transaction get() {
            return new Transaction(BigDecimal.valueOf(12.3343), LocalDateTimeFixture.NOW.get());
        }
    },

    THIRTY_SECONDS_BEFORE {
        @Override
        public Transaction get() {
            return new Transaction(BigDecimal.valueOf(12.3343), LocalDateTimeFixture.THIRTY_SECONDS_BEFORE.get());
        }
    },

    A_MINUTE_BEFORE {
        @Override
        public Transaction get() {
            return new Transaction(BigDecimal.valueOf(12.3343), LocalDateTimeFixture.A_MINUTE_BEFORE.get());
        }
    },

    A_MINUTE_AHEAD {
        @Override
        public Transaction get() {
            return new Transaction(BigDecimal.valueOf(12.3343), LocalDateTimeFixture.A_MINUTE_AHEAD.get());
        }
    }

}
