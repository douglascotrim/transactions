package com.n26.fixtures;

import java.time.LocalDateTime;
import java.util.function.Supplier;

public enum LocalDateTimeFixture implements Supplier<LocalDateTime> {

    NOW {
        @Override
        public LocalDateTime get() {
            return LocalDateTime
                    .of(2018, 7, 17, 9, 59, 51, 312000000);
        }
    },

    THIRTY_SECONDS_BEFORE {
        @Override
        public LocalDateTime get() {
            return NOW.get().minusSeconds(30);
        }
    },

    A_MINUTE_BEFORE {
        @Override
        public LocalDateTime get() {
            return NOW.get().minusMinutes(1).minusSeconds(1);
        }
    },

    A_MINUTE_AHEAD {
        @Override
        public LocalDateTime get() {
            return NOW.get().plusMinutes(1);
        }
    }

}
